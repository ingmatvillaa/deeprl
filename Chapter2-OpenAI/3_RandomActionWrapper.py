#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 18:07:20 2019

Usage of the ActionWrapper for randomly modify the space of actions of the agent

@author: mvilla
"""

import gym
import random 

class RandomActionWrapper (gym.ActionWrapper) :
    
    def __init__(self, env, Epsilon=0.1):
        
        ## Initializes the wrapper by calling parents __init__ (ActionWrapper)
        super(RandomActionWrapper, self).__init__(env) 
        
        ## The probaility of a random action
        self.Epsilon = Epsilon 
    
    def action(self,action): ## WARNING !! dont change the words 'action' nor 'environment' in any way
        
        ## We sample a random action and we will return it with a probility of epsilon 
        if random.random() < self.Epsilon:
            print("Random!")
            return self.env.action_space.sample()
        
        ## If the random number is higher than epsilon we return the Action that was suposed to be returned
        return action 
        
if __name__ == "__main__":
    
    Env = RandomActionWrapper(gym.make("CartPole-v0"),Epsilon = 0.9)   
    
    Obs = Env.reset()
    
    TotalReward = 0.0
    
    TotalSteps = 0.0
    
    while True : 
        
        Obs , Reward , Done, _ = Env.step(0)
        
        TotalReward += Reward
        TotalSteps += 1
        if Done :
            
            break
#        
    print ("Total reward : %.2f " % TotalReward )
    print ("Total Steps :  " + str(TotalSteps))
        
        
        
    
    
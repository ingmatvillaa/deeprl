#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 18:16:59 2019

@author: mvilla
"""

import gym

## We create our environment and initialize the counters of reward and total steps to 0 

if __name__=="__main__":
    
    # Load the environment
    Environment = gym.make("CartPole-v0")
    
    # Initialize the total reward 
    TotalReward = 0.0
    
    # Initialize the steps counter 
    TotalSteps = 0
    
    # Resets the environment to an initial state (must do it always)
    Observations = Environment.reset()
    
    # Perform until the episode is done
    while True:
        
        # 1. Samples a Random Action
        Action = Environment.action_space.sample() 
        
        # 2. Tells the environment which action was performed and the obtain the observations 
        # the reward and if the episode is Done 
        Observations, Reward, Done, _ = Environment.step(Action)
        
        # 3. Cumulates the total reward
        TotalReward += Reward
        
        # 4. Add a step to the episode
        TotalSteps += 1
        
        # 5. Check if the episode is done to break the loop
        if Done:
            break
        
    print "Episode done in %d steps, total reward %.2f" % (TotalSteps, TotalReward)
    
    